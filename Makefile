#!make

.SILENT:
.ONESHELL:

build:
	cd docker-compose/
	-docker-compose build

up:
	cd docker-compose/
	-docker-compose up -d

start:
	cd docker-compose/
	-docker-compose start

stop:
	cd docker-compose/
	-docker-compose stop
reg:
	cd registry/
	-./mvnw
